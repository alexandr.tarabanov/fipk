import React from 'react';
import {render} from "react-dom";
import App from './features/home/App';
import './styles/index.scss'
import * as serviceWorker from './serviceWorker';

function renderApp(app) {
    render(
        <React.StrictMode>
            {app}
        </React.StrictMode>,
        document.getElementById('fipk')
    )
}

renderApp(<App/>)

serviceWorker.unregister();
