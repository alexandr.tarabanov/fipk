export const sidebarTitles = {
    name: 'test.fi.ru',
    data: [
        {
            title: 'Тесты',
            icon: '/svg/tests.svg'
        },
        {
            title: 'Список группы',
            icon: '/svg/group.svg'
        },
        {
            title: 'О компании',
            icon: '/svg/about.svg'
        },
    ]
}