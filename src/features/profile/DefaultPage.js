import React from 'react';
import profile from '../../data/svg/profile.svg'

export default function DefaultPage() {

    return (
        <aside>
            <figure><img src={profile} alt="profile"/></figure>
        </aside>
    );
}