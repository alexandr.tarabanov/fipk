import React, {Component} from 'react'
import {stepsData} from "../../data/steps";
import 'react-tabs/style/react-tabs.css';
import StepQuestion from "./StepQuestion";
import man from "../../data/svg/man.svg";
import triangle from "../../data/svg/triangle.svg";


export default class Steps extends Component {

    state = {
        index: 1
    }


    render() {

        let tbs = []
        let tbs_content = []

        for (const step of stepsData) {
            step &&
            tbs.push(step.title)
            tbs_content.push(step.content)
        }

        return (
            <section className="steps-container">

                {/* список шагов */}
                <ul className="steps-items-container">

                    {tbs.map((step, s) => {
                        const cls = s === this.state.index ? 'active' : s < this.state.index ? 'selected' : 'disabled'
                        // const cls = s < this.state.index ? 'selected' : 'disabled'
                        return <li key={s} className={cls}>
                            {s === this.state.index || s < this.state.index ?
                                <div className='step'>{step}</div> : step}
                        </li>
                    })}
                </ul>
                <div className="steps-qa-container">

                    {/* Содержимое вкладок с вопросами по шагам */}
                    <div className="steps-qa-wrapper">
                        {tbs_content.map((item, i) => item.questions && <div key={i}>
                            <h4>{item.title}</h4>

                            {item.questions && <StepQuestion questions={item.questions}/>}

                            <div className="step-buttons-container">
                                <button className='answer'>Ответить</button>
                                <button className='change'>
                                    Заменить вопрос <img src={triangle} alt="change"/>
                                </button>
                            </div>
                        </div>)}
                    </div>

                    <div className="steps-qa-helper">
                        <div>
                            <span>Олег,&nbsp;подскажи!</span>
                        </div>
                        <figure>
                            <img src={man} alt="man"/>
                        </figure>
                    </div>

                </div>
            </section>
        )
    }
}