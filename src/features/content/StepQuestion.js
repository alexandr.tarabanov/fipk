import React from 'react';

export default function StepQuestion({questions}) {

    return (
        <div className='questions-container'>
            <ul>
                {questions.map((question, q) => <li key={q}>
                    <input className='qa-checkbox'
                           type="checkbox"
                           name={q}
                           value={question.q}/>
                    <label htmlFor={q}>{question.q}</label>
                </li>)}
            </ul>
        </div>
    );
}