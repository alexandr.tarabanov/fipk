import React from 'react';
import Title from "./Title";
import Steps from "./Steps";

export default function DefaultPage() {

    return (
        <div className='content-container'>
            <Title/>
            <Steps/>
        </div>
    );
}