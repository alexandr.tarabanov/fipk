import React from 'react';
import {default as Sidebar} from "../sidebar/DefaultPage";
import {default as Content} from "../content/DefaultPage";
import {default as Profile} from "../profile/DefaultPage";

export default function App() {
    return <div className="App">
        <Sidebar/>
        <Content/>
        <Profile/>
    </div>
}
