import React from 'react';
import {sidebarTitles} from "../../data/sidebar";

export default function DefaultPage() {

    const {name, data} = sidebarTitles;

    return (
        <div className='sidebar-container'>
            <div className='sidebar-wrapper'>

                <h4>{name}</h4>

                {data.map((item, i) => <div key={i} className={i === 0 ? 'active' : ''}>
                    <img src={item.icon} alt={item.title}/>
                    <span>{item.title}</span>
                </div>)}

            </div>
        </div>
    )

}